continent icons
===============
continent icons

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dronz/continent-icons "*"
```

or add

```
"dronz/continent-icons": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \dronz\continents\AutoloadExample::widget(); ?>```